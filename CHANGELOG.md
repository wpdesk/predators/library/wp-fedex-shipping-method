## [1.3.2] - 2024-02-15
### Fixed
- Instance fields

## [1.3.0] - 2024-02-08
### Added
- REST API support

## [1.2.0] - 2022-07-07
### Added
- Rate cache

## [1.1.1] - 2022-04-11
### Removed
- Shipping Zone settings
### Added
- Upselling

## [1.0.4] - 2022-04-08
### Fixed
- Shipping Zone settings header changed
- Fallback costs in global shipping method

## [1.0.2] - 2022-04-06
### Fixed
- Shipping Zone settings header added

## [1.0.1] - 2022-04-06
### Fixed
- settings script

## [1.0.0] - 2022-04-05
### Added
- initial version
