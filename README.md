[![pipeline status](https://gitlab.com/wpdesk/wp-fedex-shipping-method/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-fedex-shipping-method/pipelines)
[![coverage report](https://gitlab.com/wpdesk/wp-fedex-shipping-method/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-fedex-shipping-method/commits/master)
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-fedex-shipping-method/v/stable)](https://packagist.org/packages/wpdesk/wp-fedex-shipping-method)
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-fedex-shipping-method/downloads)](https://packagist.org/packages/wpdesk/wp-fedex-shipping-method)
[![License](https://poser.pugx.org/wpdesk/wp-fedex-shipping-method/license)](https://packagist.org/packages/wpdesk/wp-fedex-shipping-method)

# FedEx Shipping Method

Allows to integrate WooCommerce shipping methods interface with FedEx rates calculation mechanism and services.

This library uses the following:
- https://gitlab.com/wpdesk/predators/library/abstract-shipping
- https://gitlab.com/wpdesk/wp-woocommerce-shipping
- https://gitlab.com/wpdesk/wp-fedex-shipping-method
- https://gitlab.com/wpdesk/predators/library/fedex-shipping-service

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-fedex-shipping-method
```

## Example usage

```php
<?php

...
	public function hooks() {
		add_filter( 'woocommerce_shipping_methods', array( $this, 'add_fedex_shipping_method' ) );
	}

	/**
	 * Adds shipping method to Woocommerce.
	 *
	 * @param array $methods Methods.
	 *
	 * @return array
	 */
	public function add_fedex_shipping_method( $methods ) {
		$methods['flexible_shipping_fedex'] = \WPDesk\WooCommerceShipping\Fedex\FedexShippingMethod::class;

		return $methods;
	}

...


```

